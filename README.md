# Lexicalcorp

Repo pour le projet de benchmark des méthodes de richesse lexicale des textes littéraires - Marc Allassonnière-Tang / Antoine Silvestre de Sacy

## Références : 

- https://www.tandfonline.com/doi/full/10.1080/09296171003643098?needAccess=true
- https://journals.openedition.org/discours/9950

## Méthodologie : 

- Implémentation des calculs de richesses lexicales.
- Benchmark de ces calculs en fonction de la longueur des textes : 
    - Quelle est la mesure la plus pertinente pour le calcul de la richesse lexicale d'un texte ? Ie. quelle est la mesure la moins dépendante de la longueur d'un texte ?
    - Quelle est la mesure la plus performante en fonction du volume de textes à analyser ?
- Liaison de ce benchmark avec le coût carbone du calcul de richesse lexicale.

## Preprocessing : 

- Parmi un corpus de 3000 romans (ANR Chapitres, Aude Leblond (Paris III Sorbonne Nouvelle)), nous sélectionnons 50 romans de façon à ce que nous obtenions 50 romans qui se situent à des écarts équivalents sur la distribution générale du corpus en termes de longueur des textes.
- Le fichier `~/lexicalcorp/data/data_size.txt` contient le titre et la taille de chaque roman du corpus. 
- Nous ordonnons le copus en termes de volume des textes, nous découpons ce corpus en 10 sous-corpus équilibrés puis nous sélectionnons 5 valeurs aléatoires dans ces 10 sous-corpus de façon à ce que la sélection soit aléatoire mais respecte aussi la distribution du corpus en termes de longueur des phrases.  

